package negin.HBase;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;

public class UnionJob extends Configured implements Tool {

	//@Override
	public int run(String[] arg0) throws Exception {
		List<Scan> scans = new ArrayList<Scan>();

		Scan scan1 = new Scan();
		scan1.setAttribute("scan.attributes.table.name", Bytes.toBytes("vcf"));
		System.out.println(scan1.getAttribute("scan.attributes.table.name"));
		scans.add(scan1);

		Scan scan2 = new Scan();
		scan2.setAttribute("scan.attributes.table.name", Bytes.toBytes("annotation"));
		System.out.println(scan2.getAttribute("scan.attributes.table.name"));
		scans.add(scan2);

		Configuration conf = new Configuration();
		Job job = new Job(conf);	
		job.setJarByClass(UnionJob.class);

		TableMapReduceUtil.initTableMapperJob(
				scans, 
				UnionMapper.class, 
				Text.class, 
				IntWritable.class, 
				job);
	    TableMapReduceUtil.initTableReducerJob(
	    		"Annotated VCF Table",
	    		UnionReducer.class, 
	    		job);
	    job.waitForCompletion(true);
		return 0;
	}
	
	public static void main(String[] args) throws Exception {
		UnionJob runJob = new UnionJob();
		runJob.run(args);
	}
}
