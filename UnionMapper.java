package negin.HBase;

import java.util.Arrays;


import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

public class UnionMapper extends TableMapper<Text, IntWritable> {
	private static byte[] vcfTable = Bytes.toBytes("vcf");
	private static byte[] annotationTable = Bytes.toBytes("annotation");
	byte[] seq;
	String vcfBase;
	String vBase;
	int		VBASE;
	String annotationBase;
	String aBase;
	int 	ABASE;
	Text mapperKey;
	IntWritable mapperValue;

	@Override
	public void map(ImmutableBytesWritable rowKey, Result columns, Context context) {
		// get table name
		TableSplit currentSplit = (TableSplit)context.getInputSplit();
		byte[] tableName = currentSplit.getTableName();
		try {
			if (Arrays.equals(tableName, vcfTable)) {
				String geneID = new String(rowKey.get());
				seq = columns.getValue(Bytes.toBytes("base"), Bytes.toBytes("vBase"));
				vcfBase = new String(seq);
				VBASE = new Integer(vcfBase);
				mapperKey = new Text("v#"+geneID);
				mapperValue = new IntWritable(VBASE);
				context.write(mapperKey, mapperValue);
			} else if (Arrays.equals(tableName, annotationTable)) {
				String geneID = new String(rowKey.get());
				seq = columns.getValue(Bytes.toBytes("base"), Bytes.toBytes("aBase"));
				aBase = new String(seq);
				ABASE = new Integer("a#"+aBase);
				mapperKey = new Text(geneID);
				mapperValue = new IntWritable(ABASE);
				context.write(mapperKey, mapperValue);
			}
		} catch (Exception e) {
			// TODO : exception handling logic
			e.printStackTrace();
		}
	}
}