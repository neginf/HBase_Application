package negin.HBase;

import java.io.IOException;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;

public class UnionReducer extends TableReducer<Text, IntWritable, ImmutableBytesWritable>{
	 @Override
	 public void reduce(Text key, Iterable<IntWritable> values, Context context)  {
		 if (key.toString().startsWith("v")) {
			 Integer repeat = 0;
			 for (IntWritable counter : values) {
				 repeat = repeat + new Integer(storeSale.toString());
			 }
			 Put put = new Put(Bytes.toBytes(key.toString()));
			 put.add(Bytes.toBytes("base"), Bytes.toBytes("REPEATED"), Bytes.toBytes(repeat));
			 try {
				context.write(null, put);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 } else {
			 Integer repeat = 0;
			 for (IntWritable counter : values) {
				 repeat = repeat + new Integer(counter.toString());
			 }
			 Put put = new Put(Bytes.toBytes(key.toString()));
			 put.add(Bytes.toBytes("base"), Bytes.toBytes("REPEATED"), Bytes.toBytes(repeat));
			 try {
				context.write(null, put);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
	 }
}

